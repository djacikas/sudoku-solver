﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesseract;


namespace Sudoku_Solver
{
   class SkaiciuAtpazinimas
   {
     

      public void SkaiciuAtpazinimasMetodas(SudokuData sudokuData, string apdorotasImg)
      {
         //int[,] sudoku = new int[9, 9];
         var testImagePath = Environment.CurrentDirectory + "\\" + apdorotasImg;
         //if (args.Length > 0)
         //{
         //   testImagePath = args[0];
         //}

         try
         {
            using (var engine = new TesseractEngine(@"./tessdata", "eng", EngineMode.Default))
            {
               using (var img = Pix.LoadFromFile(testImagePath))
               {
                  engine.SetVariable("tessedit_char_whitelist", "0123456789");
                  var xw = img.Width / 9;
                  var yh = img.Height / 9;
                  for (int i = 0; i < 9; i++)
                  {
                     for (int j = 0; j < 9; j++)
                     {

                        using (var page = engine.Process(img, new Rect(i * xw, (8-j) * yh, xw, yh), PageSegMode.SingleChar))
                        {
                           var skaicius = 0;
                           var text = page.GetText();
                           skaicius = int.TryParse(text, out skaicius) ? skaicius : 0;
                           sudokuData.kubas[i, j, 0] = skaicius;
                           //sudoku[i, j] = skaicius;

                        }
                     }
                  }
               }
            }
         }
         catch (Exception e)
         {
            Trace.TraceError(e.ToString());
            Console.WriteLine("Unexpected Error: " + e.Message);
            Console.WriteLine("Details: ");
            Console.WriteLine(e.ToString());
         }
         //for (int y = 0; y < 9; y++)
         //{
         //   for (int x = 0; x < 9; x++)
         //   {
         //      Console.Write(sudoku[x, y] + " ");
         //      if (x == 2 || x == 5) Console.Write(" ");
         //   }
         //   Console.WriteLine();
         //   if (y == 2 || y == 5) Console.WriteLine();
         //}
      }
   }
}
