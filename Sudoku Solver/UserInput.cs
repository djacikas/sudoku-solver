﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sudoku_Solver
{
   public partial class UserInput : UserControl
   {
      ApdorotiFoto apdorotiFoto = new ApdorotiFoto();
      SkaiciuAtpazinimas skaiciuAtpazinimas = new SkaiciuAtpazinimas();
      SudokuData sudokuData = new SudokuData();

      Point location;

      public UserInput(Point location)
      {
         InitializeComponent();
         this.location = location;
      }

      //public Point location = new Point(0,0);

      public event Action<Point, int> MouseClick;

      public void OnMouseClick(Point location, int skaicius)
      {
         //if (MouseClick != null)
         //{
         //   MouseClick(location, skaicius);
         //}

         MouseClick?.Invoke(location, skaicius);
      }


      // Pagrindinė skaičiaus reikšmė
      private void button10_Click(object sender, EventArgs e)
      {
         if (button10.Visible)
         {
            button10.Text = "0";
            button10.Visible = false;
            OnMouseClick(location, 0);
         }

      }

      public void SetButtonValue(int value)
      {
         if (value != 0)
         {
            button10.Text = Convert.ToString(value);
            button10.Visible = true;
         }
         else if (value == 0)
         {
            button10.Text = Convert.ToString(value);
            button10.Visible = false;
         }
         OnMouseClick(location, value);
      }



      // Mažojo 1 skaičiaus reikšmė
      private void button1_Click(object sender, EventArgs e)
      {
         SetButtonValue(1);

      }

      private void button2_MouseClick(object sender, MouseEventArgs e)
      {
         SetButtonValue(2);

      }

      private void button3_MouseClick(object sender, MouseEventArgs e)
      {
         SetButtonValue(3);

      }

      private void button4_MouseClick(object sender, MouseEventArgs e)
      {
         SetButtonValue(4);

      }

      private void button5_MouseClick(object sender, MouseEventArgs e)
      {
         SetButtonValue(5);

      }

      private void button6_MouseClick(object sender, MouseEventArgs e)
      {
         SetButtonValue(6);

      }

      private void button7_MouseClick(object sender, MouseEventArgs e)
      {
         SetButtonValue(7);

      }

      private void button8_MouseClick(object sender, MouseEventArgs e)
      {
         SetButtonValue(8);

      }

      private void button9_MouseClick(object sender, MouseEventArgs e)
      {
         SetButtonValue(9);

      }
   }
}
