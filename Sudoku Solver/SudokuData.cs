﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sudoku_Solver
{
   public class SudokuData
   {
      public int[,,] kubas = new int[9, 9, 10]; // x, y, z
      int[] eiluciuSumos = new int[9];
      int[] stulpeliuSumos = new int[9];
      int[] mazesniuKvadratuSkaiciai = new int[9];


      //public SudokuData()
      //{
      //   Duomenys(kubas);
      //}

      public void Tikrinimas()
      {
         for (int skaicius = 1; skaicius < kubas.GetLength(2); skaicius++)
         {
            NunulintiMatrica(skaicius);
            for (int y = kubas.GetLength(1) - 1; y >= 0; y--)
            {
               for (int x = 0; x < kubas.GetLength(0); x++)
               {
                  if (kubas[x, y, 0] == 0)
                  {
                     bool galima = true;
                     // patikrinam ar yra skaiciaus reiksmiu eiluteje
                     for (int xx = 0; xx < kubas.GetLength(0) && galima; xx++)
                     {
                        if (kubas[xx, y, 0] == skaicius)
                        {
                           galima = false;
                        }
                     }
                     // patikrinam ar yra skaiciaus reiksmiu stulpelyje
                     for (int yy = kubas.GetLength(0) - 1; yy >= 0 && galima; yy--)
                     {
                        if (kubas[x, yy, 0] == skaicius)
                        {
                           galima = false;
                        }
                     }
                     // patikrinam ar yra skaiciaus reiksmiu mazajame kvadrate
                     for (int xx = (x / 3) * 3; xx < (x / 3) * 3 + 3; xx++)
                     {
                        for (int yy = (y / 3) * 3; yy < (y / 3) * 3 + 3; yy++)
                        {
                           if (kubas[xx, yy, 0] == skaicius)
                           {
                              galima = false;
                           }
                        }
                     }
                     if (galima) kubas[x, y, skaicius] = skaicius;
                  }
               }
            }
         }
      }

      public bool Irasymas()
      {
         bool pakeiciau = false;
         for (int skaicius = 1; skaicius < kubas.GetLength(2); skaicius++)
         {
            for (int y = kubas.GetLength(1) - 1; y >= 0; y--)
            {
               for (int x = 0; x < kubas.GetLength(0); x++)
               {
                  if (kubas[x, y, skaicius] == skaicius)
                  {
                     var count = 0;
                     // patikrinam ar yra skaicius yra vienintelis eiluteje
                     for (int xx = 0; xx < kubas.GetLength(0) && count <= 1; xx++)
                     {
                        if (kubas[xx, y, skaicius] == skaicius)
                        {
                           count++;
                        }
                     }
                     if (count == 1)
                     {
                        kubas[x, y, 0] = skaicius;
                        //NunulintiMatrica(skaicius);
                        NunulintiEiluteStulpeliKvadrata(x, y, skaicius);
                        return true;
                     }
                     count = 0;
                     // patikrinam ar yra skaicius yra vienintelis stulpelyje
                     for (int yy = kubas.GetLength(0) - 1; yy >= 0 && count <= 1; yy--)
                     {
                        if (kubas[x, yy, skaicius] == skaicius)
                        {
                           count++;
                        }
                     }
                     if (count == 1)
                     {
                        kubas[x, y, 0] = skaicius;
                        //NunulintiMatrica(skaicius);
                        NunulintiEiluteStulpeliKvadrata(x, y, skaicius);
                        return true;
                     }
                     count = 0;
                     // patikrinam ar yra skaicius yra vienintelis mazajame kvadrate
                     for (int xx = (x / 3) * 3; xx < (x / 3) * 3 + 3; xx++)
                     {
                        for (int yy = (y / 3) * 3; yy < (y / 3) * 3 + 3; yy++)
                        {
                           if (kubas[xx, yy, skaicius] == skaicius)
                           {
                              count++;
                           }
                        }
                     }
                     if (count == 1)
                     {
                        kubas[x, y, 0] = skaicius;
                        //NunulintiMatrica(skaicius);
                        NunulintiEiluteStulpeliKvadrata(x, y, skaicius);
                        return true;
                     }

                  }
               }
            }
         }
         return pakeiciau;
      }

      // nunulinti visa pasirinkta matrica
      public void NunulintiMatrica(int skaicius)
      {
         for (int y = kubas.GetLength(1) - 1; y >= 0; y--)
         {
            for (int x = 0; x < kubas.GetLength(0); x++)
            {
               kubas[x, y, skaicius] = 0;
            }
         }
      }

      // nunulinti visas skaiciu nuo 1 iki 9 x,y koordinaciu eilutes, stulpelius ir maz. kvadratus 
      // kogero negerai, kad nunulinu kitu skaiciu, nei irasytas, eilutes ir stulpelius, nes juose tos reiksmes gali buti teisingos. Reiktu perduoti 3 parametrus, nunulinti irasyto skaiciaus eil, stulp ir maz.kvadrata, o tada skaicius nuo 1 iki 9 i gyli.
      public void NunulintiEiluteStulpeliKvadrata(int x, int y, int skaicius)
      {
         for (int i = 0; i < kubas.GetLength(0); i++)
         {
            kubas[i, y, skaicius] = 0;
            kubas[x, i, skaicius] = 0;
         }

         for (int xx = (x / 3) * 3; xx < (x / 3) * 3 + 3; xx++)
         {
            for (int yy = (y / 3) * 3; yy < (y / 3) * 3 + 3; yy++)
            {
               kubas[xx, yy, skaicius] = 0;
            }
         }

         for (int gylis = 1; gylis < kubas.GetLength(2); gylis++)
         {
            kubas[x, y, gylis] = 0;
         }

      }

      //tikirina ar z matricose nuo 1 iki 9 yra irasyta tik viena potenciali reiksme, jei taip iraso skaiciu i 0 matrica
      public bool TikrinimasIrIrasymasIGyli()
      {
         bool pakeiciau = false;
         for (int x = 0; x < kubas.GetLength(0); x++)
         {
            for (int y = kubas.GetLength(1) - 1; y >= 0; y--)
            {
               var skaiciuKiekis = 0;
               var kurisSkaicius = 0;

               for (int skaicius = 1; skaicius < kubas.GetLength(2); skaicius++)
               {
                  if (kubas[x, y, skaicius] != 0)
                  {
                     skaiciuKiekis++;
                     kurisSkaicius = skaicius;
                  }
               }

               if (skaiciuKiekis == 1)
               {
                  kubas[x, y, 0] = kurisSkaicius;
                  // Pasira6yti funkcija, kuri nunulintu tik irasyto skaiciaus eilute, stulpeli ir mazaji kvadrata.
                  //NunulintiMatrica(kurisSkaicius);
                  NunulintiEiluteStulpeliKvadrata(x, y, kurisSkaicius);
                  return true;
               }
            }
         }
         return pakeiciau;
      }

      // Jei skaičiaus reikšmių mažajame kvadrate yra toje pačioje eilutėje ar stulpelyje - ištrinti kitas reikšmes analogiškai iš kitų mažųjų kvadratų, kurie taip pat priklauso tai pačiai eilutei ar stulpeliui. (reikšmes kurias reikia palikti galima išsaugoti, nunulinti matricą ir įrašyti išsaugotas reikšmes į potencialių skaičių matricą).
      public bool MazujuKvadratuTikrinimas()
      {
         var pakeiciau = false;
         var galimosKoordinates = new List<Tuple<int, int, int>>();
         //var isfiltruotosGalimosKoordinates = new List<Tuple<int, int, int>>();

         for (int skaicius = 1; skaicius < kubas.GetLength(2); skaicius++)
         {
            for (int y = kubas.GetLength(1) - 1; y >= 0; y -= 3)
            {
               for (int x = 0; x < kubas.GetLength(0); x += 3)
               {
                  for (int xx = x; xx < x + 3; xx++)
                  {
                     for (int yy = y; yy > y - 3; yy--)
                     {
                        if (kubas[xx, yy, skaicius] == skaicius)
                        {
                           galimosKoordinates.Add(new Tuple<int, int, int>(xx, yy, skaicius));
                        }
                     }
                  }

                  if (galimosKoordinates.Count == 1)
                  {
                     var xKord = galimosKoordinates[0].Item1;
                     var yKord = galimosKoordinates[0].Item2;
                     var zKord = galimosKoordinates[0].Item3;

                     kubas[xKord, yKord, 0] = zKord;
                     NunulintiEiluteStulpeliKvadrata(xKord, yKord, zKord);
                     galimosKoordinates.Clear();
                     return true;
                  }
                  else if (galimosKoordinates.Count > 1)
                  {
                     bool vienodiX = true;
                     bool vienodiY = true;
                     for (int j = 1; j < galimosKoordinates.Count; j++) // saraso ilgis
                     {
                        if (galimosKoordinates[j].Item1 != galimosKoordinates[j - 1].Item1)
                        {
                           vienodiX = false;
                        }
                        if (galimosKoordinates[j].Item2 != galimosKoordinates[j - 1].Item2)
                        {
                           vienodiY = false;
                        }
                     }

                     if (vienodiX)
                     {
                        var xKord = galimosKoordinates[0].Item1;
                        var yKord = galimosKoordinates[0].Item2;
                        var zKord = galimosKoordinates[0].Item3;

                        for (int j = 0; j < kubas.GetLength(0); j++)
                        {
                           kubas[j, yKord, zKord] = 0;
                        }

                        for (int i = 0; i < galimosKoordinates.Count; i++)
                        {
                           xKord = galimosKoordinates[i].Item1;
                           yKord = galimosKoordinates[i].Item2;
                           zKord = galimosKoordinates[i].Item3;

                           kubas[xKord, yKord, zKord] = zKord;
                           pakeiciau = true;
                        }
                     }
                     else if (vienodiY)
                     {
                        var xKord = galimosKoordinates[0].Item1;
                        var yKord = galimosKoordinates[0].Item2;
                        var zKord = galimosKoordinates[0].Item3;

                        for (int j = 0; j < kubas.GetLength(1); j++)
                        {
                           kubas[xKord, j, zKord] = 0;
                        }

                        for (int i = 0; i < galimosKoordinates.Count; i++)
                        {
                           xKord = galimosKoordinates[i].Item1;
                           yKord = galimosKoordinates[i].Item2;
                           zKord = galimosKoordinates[i].Item3;

                           kubas[xKord, yKord, zKord] = zKord;
                           pakeiciau = true;
                        }
                     }

                  }

                  galimosKoordinates.Clear();

               }
            }

            //NunulintiMatrica(skaicius);

            //for (int i = 0; i < isfiltruotosGalimosKoordinates.Count; i++)
            //{
            //    var xKord = isfiltruotosGalimosKoordinates[i].Item1;
            //    var yKord = isfiltruotosGalimosKoordinates[i].Item2;
            //    var zKord = isfiltruotosGalimosKoordinates[i].Item3;
            //    kubas[xKord, yKord, zKord] = zKord;
            //    pakeiciau = true;
            //}
         }

         return pakeiciau;

         //foreach (var i in galimosKoordinates)
         //{
         //    Console.WriteLine(i);


         //}
         //Console.WriteLine();
         //Console.WriteLine(galimosKoordinates[0].Item1);

      }

      ////////////////////////////// SPAUSDINIMAS Į KONSOLĘ
      public void Spausdinti3d()
      {
         for (int skaicius = 0; skaicius < kubas.GetLength(2); skaicius++)
         {
            for (int y = kubas.GetLength(1) - 1; y >= 0; y--)
            {
               for (int x = 0; x < kubas.GetLength(0); x++)
               {
                  if (x > 0 && x % 3 == 0) Console.Write(' ');
                  Console.Write(kubas[x, y, skaicius]);
               }
               if (y < kubas.GetLength(1) - 1 && y % 3 == 0) Console.WriteLine();
               Console.WriteLine();
            }
            Console.WriteLine();
         }
      }

      public void Spausdinti1(int skaicius)
      {
         for (int y = kubas.GetLength(1) - 1; y >= 0; y--)
         {
            for (int x = 0; x < kubas.GetLength(0); x++)
            {
               if (x > 0 && x % 3 == 0) Console.Write(' ');
               Console.Write(kubas[x, y, skaicius]);
            }
            if (y < kubas.GetLength(1) - 1 && y % 3 == 0) Console.WriteLine();
            Console.WriteLine();

         }
      }

      //public int[,,] Duomenys(int[,,] kubas)
      //{
      //   kubas[0, 0, 0] = 5;
      //   kubas[6, 0, 0] = 3;
      //   kubas[2, 1, 0] = 3;
      //   kubas[4, 1, 0] = 8;
      //   kubas[7, 1, 0] = 1;
      //   kubas[1, 2, 0] = 1;
      //   kubas[2, 2, 0] = 6;
      //   kubas[4, 2, 0] = 3;
      //   kubas[5, 2, 0] = 7;
      //   kubas[2, 3, 0] = 9;
      //   kubas[3, 3, 0] = 8;
      //   kubas[7, 3, 0] = 7;
      //   kubas[0, 4, 0] = 3;
      //   kubas[2, 4, 0] = 8;
      //   kubas[4, 4, 0] = 2;
      //   kubas[6, 4, 0] = 6;
      //   kubas[8, 4, 0] = 4;
      //   kubas[1, 5, 0] = 4;
      //   kubas[5, 5, 0] = 3;
      //   kubas[6, 5, 0] = 9;
      //   kubas[3, 6, 0] = 2;
      //   kubas[4, 6, 0] = 7;
      //   kubas[6, 6, 0] = 4;
      //   kubas[7, 6, 0] = 3;
      //   kubas[1, 7, 0] = 2;
      //   kubas[4, 7, 0] = 6;
      //   kubas[6, 7, 0] = 5;
      //   kubas[2, 8, 0] = 4;
      //   kubas[8, 8, 0] = 6;

      //   return kubas;
      //}
   }
}
