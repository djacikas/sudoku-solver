﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Sudoku_Solver
{
   public partial class Form1 : Form
   {
      ApdorotiFoto apdorotiFoto = new ApdorotiFoto();
      SkaiciuAtpazinimas skaiciuAtpazinimas = new SkaiciuAtpazinimas();
      SudokuData sudokuData = new SudokuData();
      SprestiSudoku sprestiSudoku = new SprestiSudoku();

      UserInput[,] mygtukuMatrica = new UserInput[9, 9];


      public Form1()
      {
         InitializeComponent();

         for (int y = 8; y >= 0; y--)
         {
            for (int x = 0; x < tableLayoutPanel1.RowCount; x++)
            {
               UserInput userInput = new UserInput(new Point(x, y));
               userInput.Dock = DockStyle.Fill;
               tableLayoutPanel1.Controls.Add(userInput);
               mygtukuMatrica[x, y] = userInput;
               userInput.MouseClick += UserInput_MouseClick;
            }
         }



      }

      public void AtnaujintiVaizda(int skaicius)
      {
         for (int y = tableLayoutPanel1.ColumnCount - 1; y >= 0; y--)
         {
            for (int x = 0; x < tableLayoutPanel1.RowCount; x++)
            {

               mygtukuMatrica[x, y].SetButtonValue(sudokuData.kubas[x, y, skaicius]);

            }
         }
      }



      private void UserInput_MouseClick(Point arg1, int arg2)
      {
         // keisti sudoku matricos logikoje
         var x = arg1.X;
         var y = arg1.Y;
         sudokuData.kubas[x, y, 0] = arg2;

      }

      private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
      {
         var height = tableLayoutPanel1.Height;
         var width = tableLayoutPanel1.Width;
         Pen plonaJuodaLinija = new Pen(Color.Black, 1);
         Pen storaJuodaLinija = new Pen(Color.Black, 3);

         for (int x = 0; x <= width; x += (width / 9))
         {

            if (x == 0)
               e.Graphics.DrawLine(storaJuodaLinija, x + 1, 0, x + 1, height);
            else if ((width / 3) == x || (width / 3) * 2 == x)
               e.Graphics.DrawLine(storaJuodaLinija, x, 0, x, height);
            else if ((width / 9) * 9 == x)
               e.Graphics.DrawLine(storaJuodaLinija, x, 0, x, height);
            else
               e.Graphics.DrawLine(plonaJuodaLinija, x, 0, x, height);

         }

         for (int y = 0; y < height; y += (height / 9))
         {
            if (y == 0)
               e.Graphics.DrawLine(storaJuodaLinija, 0, y + 1, width, y + 1);
            else if ((height / 3) == y || (height / 3) * 2 == y)
               e.Graphics.DrawLine(storaJuodaLinija, 0, y, width, y);
            else if ((height / 9) * 9 == y)
               e.Graphics.DrawLine(storaJuodaLinija, 0, y, width, y);
            else
               e.Graphics.DrawLine(plonaJuodaLinija, 0, y, width, y);
         }
      }

      public void tableLayoutPanel1_MouseClick(object sender, MouseEventArgs e)
      {
         //var x = e.X;
         //var y = e.Y;
         //Point location = new Point(x, y);
      }

      private void ikeltiFoto_Click(object sender, EventArgs e)
      {

         if (openFileDialog1.ShowDialog() == DialogResult.OK)
         {
            var copiedFileName = "Temp_Sudoku_Copy.png";
            var originalFilePath = openFileDialog1.FileName;
            File.Copy(originalFilePath, copiedFileName, true);

            var fullFilePath = Environment.CurrentDirectory + "\\" + copiedFileName;
            


            label1.Visible = true;
            var apdorotasImg = apdorotiFoto.ApdorotiFotoMetodas(fullFilePath);
            skaiciuAtpazinimas.SkaiciuAtpazinimasMetodas(sudokuData, apdorotasImg);
            AtnaujintiVaizda(0);
         }
      }

      private void sprestiButton_Click(object sender, EventArgs e)
      {
         label1.Visible = false;
         sprestiSudoku.SprestiSudokuMetodas(sudokuData);
         AtnaujintiVaizda(0);
      }

      private void resetButton_Click(object sender, EventArgs e)
      {
         for (int i = 0; i < sudokuData.kubas.GetLength(2); i++)
         {
            sudokuData.NunulintiMatrica(i);
         }
         AtnaujintiVaizda(0);
      }
   }
}
