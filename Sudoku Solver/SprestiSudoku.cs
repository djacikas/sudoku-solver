﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sudoku_Solver
{
   public class SprestiSudoku
   {
      public void SprestiSudokuMetodas(SudokuData sudokuData)
      {
         var matrica = sudokuData;

         matrica.Tikrinimas();
         matrica.Spausdinti1(0);
         Console.WriteLine();
         matrica.Spausdinti3d();

         var pakitimai = false;
         do
         {
            pakitimai = false;

            pakitimai = pakitimai || matrica.Irasymas();
            matrica.Spausdinti1(0);
            Console.WriteLine();
            matrica.Spausdinti3d();

            ////mazuju kvadratu tikrinime yra klaida(be ju, issprendzia)
            //matrica.MazujuKvadratuTikrinimas();
            //matrica.Spausdinti1(0);
            //Console.WriteLine();
            //matrica.Spausdinti3d();

            pakitimai = pakitimai || matrica.TikrinimasIrIrasymasIGyli();
            matrica.Spausdinti1(0);
            Console.WriteLine();

         } while (pakitimai);
      }
   }
}
