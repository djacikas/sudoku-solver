﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenCvSharp;

namespace Sudoku_Solver
{
   public partial class CropImg : Form
   {
      public CropImg()
      {
         InitializeComponent();
      }

      private void button1_Click(object sender, EventArgs e)
      {
         using (var src = new IplImage(filePath, LoadMode.Color))
         using (var gray = new IplImage(src.Size, BitDepth.U8, 1))
         using (var canny = new IplImage(src.Size, BitDepth.U8, 1))
         using (var result = src.Clone())
         {
            // detect edges
            Cv2.CvtColor(src, gray, ColorConversion.BgrToGray);
            Cv2.Canny(gray, canny, 50, 200);

            // find all contours
            using (CvMemStorage storage = new CvMemStorage())
            {
               // find contours by CvContourScanner

               // native style
               /*
               CvContourScanner scanner = Cv.StartFindContours(canny, storage, CvContour.SizeOf, ContourRetrieval.Tree, ContourChain.ApproxSimple);
               while (true)
               {
                   CvSeq<CvPoint> c = Cv.FindNextContour(scanner);
                   if (c == null)
                       break;
                   else
                       Cv.DrawContours(result, c, CvColor.Red, CvColor.Green, 0, 3, LineType.AntiAlias);
               }
               Cv.EndFindContours(scanner);
               //*/

               // wrapper style
               using (CvContourScanner scanner = new CvContourScanner(canny, storage, CvContour.SizeOf, ContourRetrieval.Tree, ContourChain.ApproxSimple))
               {
                  foreach (CvSeq<CvPoint> c in scanner)
                  {
                     result.DrawContours(c, CvColor.Red, CvColor.Green, 0, 3, LineType.AntiAlias);
                  }
               }
            }

            // show canny and result
            using (new CvWindow("ContourScanner canny", canny))
            using (new CvWindow("ContourScanner result", result))
            {
               Cv.WaitKey();
            }
         }
      }
   }
}
