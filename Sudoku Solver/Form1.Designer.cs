﻿namespace Sudoku_Solver
{
   partial class Form1
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
         this.ikeltiFoto = new System.Windows.Forms.Button();
         this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
         this.sprestiButton = new System.Windows.Forms.Button();
         this.label1 = new System.Windows.Forms.Label();
         this.resetButton = new System.Windows.Forms.Button();
         this.SuspendLayout();
         // 
         // tableLayoutPanel1
         // 
         this.tableLayoutPanel1.ColumnCount = 9;
         this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
         this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
         this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
         this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
         this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
         this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
         this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
         this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
         this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 47F));
         this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
         this.tableLayoutPanel1.Name = "tableLayoutPanel1";
         this.tableLayoutPanel1.RowCount = 9;
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
         this.tableLayoutPanel1.Size = new System.Drawing.Size(407, 407);
         this.tableLayoutPanel1.TabIndex = 1;
         this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
         this.tableLayoutPanel1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tableLayoutPanel1_MouseClick);
         // 
         // ikeltiFoto
         // 
         this.ikeltiFoto.Location = new System.Drawing.Point(425, 13);
         this.ikeltiFoto.Name = "ikeltiFoto";
         this.ikeltiFoto.Size = new System.Drawing.Size(169, 23);
         this.ikeltiFoto.TabIndex = 2;
         this.ikeltiFoto.Text = "Įkelti Foto";
         this.ikeltiFoto.UseVisualStyleBackColor = true;
         this.ikeltiFoto.Click += new System.EventHandler(this.ikeltiFoto_Click);
         // 
         // openFileDialog1
         // 
         this.openFileDialog1.FileName = "openFileDialog1";
         // 
         // sprestiButton
         // 
         this.sprestiButton.Location = new System.Drawing.Point(425, 121);
         this.sprestiButton.Name = "sprestiButton";
         this.sprestiButton.Size = new System.Drawing.Size(169, 23);
         this.sprestiButton.TabIndex = 3;
         this.sprestiButton.Text = "Spręsti Sudoku";
         this.sprestiButton.UseVisualStyleBackColor = true;
         this.sprestiButton.Click += new System.EventHandler(this.sprestiButton_Click);
         // 
         // label1
         // 
         this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
         this.label1.Location = new System.Drawing.Point(425, 39);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(169, 62);
         this.label1.TabIndex = 4;
         this.label1.Text = "Patikrinkite ar duomenys atpažinti teisingai";
         this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.label1.Visible = false;
         // 
         // resetButton
         // 
         this.resetButton.Location = new System.Drawing.Point(425, 329);
         this.resetButton.Name = "resetButton";
         this.resetButton.Size = new System.Drawing.Size(169, 23);
         this.resetButton.TabIndex = 5;
         this.resetButton.Text = "Nunulinti duomenis";
         this.resetButton.UseVisualStyleBackColor = true;
         this.resetButton.Click += new System.EventHandler(this.resetButton_Click);
         // 
         // Form1
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(606, 427);
         this.Controls.Add(this.resetButton);
         this.Controls.Add(this.label1);
         this.Controls.Add(this.sprestiButton);
         this.Controls.Add(this.ikeltiFoto);
         this.Controls.Add(this.tableLayoutPanel1);
         this.ForeColor = System.Drawing.SystemColors.ControlText;
         this.Name = "Form1";
         this.Text = "Form1";
         this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
      private System.Windows.Forms.Button ikeltiFoto;
      private System.Windows.Forms.OpenFileDialog openFileDialog1;
      private System.Windows.Forms.Button sprestiButton;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Button resetButton;
   }
}

