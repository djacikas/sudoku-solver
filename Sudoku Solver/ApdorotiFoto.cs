﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenCvSharp;
using System.Drawing;
using System.IO;

namespace Sudoku_Solver
{
   class ApdorotiFoto
   {
      public string ApdorotiFotoMetodas(string fullFilePath)
      {
         //var fileName = form1.fileName;
         //var directoryName = form1.directoryName;

         //var fullFilePath = directoryName + fileName;

         var tempSudokuBinaryFile = "Temp_Sudoku_Binary.png";
         var directoryName = Environment.CurrentDirectory;
         var apdorotasImg = "Temp_Sudoku_Pagrindine_Matrica_Data.png";
         var bitNuo = 175; //default 175

         var img = Cv2.ImRead(fullFilePath, ImreadModes.GrayScale);

         var binary = img.Threshold(bitNuo, 255, ThresholdTypes.Binary);
         binary.SaveImage(tempSudokuBinaryFile);
         Bitmap bmp = new Bitmap(tempSudokuBinaryFile);
         bool[,] visited = new bool[bmp.Width, bmp.Height];
         Queue<System.Drawing.Point> queue = new Queue<System.Drawing.Point>();
         queue.Enqueue(new System.Drawing.Point(0, 0));
         visited[0, 0] = true;
         while (queue.Count > 0)
         {
            var point = queue.Dequeue();
            bmp.SetPixel(point.X, point.Y, Color.White);
            for (int x = -1; x <= 1; x++)
            {
               for (int y = -1; y <= 1; y++)
               {
                  var newX = point.X + x;
                  var newY = point.Y + y;
                  if (newX >= 0 && newX < bmp.Width
                     && newY >= 0 && newY < bmp.Height
                     && !visited[newX, newY] && (bmp.GetPixel(newX, newY).R - 15) <= 0)
                  {
                     queue.Enqueue(new System.Drawing.Point(newX, newY));
                     visited[newX, newY] = true;
                  }
               }
            }
         }

         bmp.Save(apdorotasImg, System.Drawing.Imaging.ImageFormat.Png);
         File.Delete(fullFilePath);
         //File.Delete(directoryName + "\\" + tempSudokuBinaryFile);

         return apdorotasImg;
      }

   }
}
